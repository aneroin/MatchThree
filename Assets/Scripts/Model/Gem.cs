﻿public class Gem {
    
    // Types of the gems present in a game
    public enum Kind {
        red = 0,
        blue = 1,
        green = 2,
        yellow = 3
    }

    private Matrix _parent;
    public Matrix parent {
        get { return _parent; }
    }
    private Kind _kind;
    public Kind kind {
        get { return _kind; }
    }

    public Gem(Kind kind, Matrix parent){
        _kind = kind;
        _parent = parent;
    }

    public static Gem Random(Matrix parent){
        return new Gem((Kind) UnityEngine.Random.Range(0,4),parent);
    }

    public bool IsSame(Kind otherKind){
        return kind == otherKind;
    }
    
    public bool IsSame(Gem otherGem)
    {
        if (otherGem == null)
            return false;
        return IsSame(otherGem.kind);
    }
}