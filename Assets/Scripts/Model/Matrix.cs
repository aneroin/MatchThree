﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

public class Matrix {

  private int w;
  public int width { get { return w; } }
  private int h;
  public int height { get { return h; } }

  public Action<Gem> OnSpawn;
  public Action<int, int, int, int> OnSwap;
  public Action<Gem> OnDestroy;

  Gem[] gems;

  public Matrix(int w, int h){
    this.w = w;
    this.h = h;
    gems = new Gem[w * h];
  }

  public Gem GetGem(int x, int y){
    return gems[y * width + x];
  }

  public void PutGem(int x, int y, Gem gem){
    Debug.Log("Putting gem at "+x+":"+y);
    gems[y * width + x] = gem;
    if (OnSpawn != null)
      OnSpawn(gem);
  }

  public void DestroyGem(Gem gem)
  {
    int x, y;
    if (GetGemCoords(gem, out x, out y))
    {
      if (OnDestroy!=null)
        OnDestroy(gem);
      gems[y * width + x] = null;
    }
  }

  public bool GetGemCoords(Gem gem, out int x, out int y){
    x = y = 0;
    int index = Array.IndexOf(gems, gem);
    if (index < 0){
      return false;
    } else {
      x = index % width;
      y = index / width;
      return true;
    }
  }

  public bool TrySwapGems(Gem one, Gem another){
    int one_x, one_y;
    int another_x, another_y;

    if (
      GetGemCoords(one, out one_x, out one_y) && // if matrix can get coords of gem one
      GetGemCoords(another, out another_x, out another_y) // and if matrix can get coords of gem another
    ) {
      if (one_x != another_x && one_y == another_y) // gems on vertical line
      if (Mathf.Abs(one_x - another_x) == 1) // gems are located adjacent
      {
        // valid swap
        SwapGems(one, another);
        return true;
      }

      if (one_y != another_y && one_x == another_x) // gems on horizontal line
      if (Mathf.Abs(one_y - another_y) == 1) // gems are located adjacent
      {
        // valid swap
        SwapGems(one, another);
        return true;
      }
    }

    // else swap is not valid
    return false;
  }

  private void SwapGems(Gem one, Gem another)
  {
    int one_x, one_y, another_x, another_y;
    GetGemCoords(one, out one_x, out one_y);
    GetGemCoords(another, out another_x, out another_y);
    if (OnSwap != null)
      OnSwap(one_x, one_y, another_x, another_y);
    int one_index = Array.IndexOf(gems, one);
    int another_index = Array.IndexOf(gems, another);
    Gem temporary = gems[one_index];
    gems[one_index] = gems[another_index];
    gems[another_index] = temporary;
  }
  
  private void SwapGems(int one_x, int one_y, int another_x, int another_y)
  {
    if (OnSwap != null)
      OnSwap(one_x, one_y, another_x, another_y);
    int one_index = one_y * width + one_x;
    int another_index = another_y * width + another_x;
    Gem temporary = gems[one_index];
    gems[one_index] = gems[another_index];
    gems[another_index] = temporary;
  }

  public List<Gem> GetCandidateGems(){
    List<Gem> candidateGems = new List<Gem>();
    for (int i = 0; i < width; i++)
      for (int k = 0; k < height; k++)
      {
        if (GetGem(i, k) == null) continue;
        // for each element in the array of gems, check if particular one has same horiaontal or vertical neighbours
        if (GetGem(i,k).HasVerticalNeighbours() || GetGem(i,k).HasHorizontalNeighbours())
        {
          // add valid gem to the list
          candidateGems.Add(GetGem(i,k));
        }
      }
    // return candidate gems to process further
    return candidateGems;
  }

  public int[] FallGems()
  {
    int[] gaps = new int[width];
    for (int x = 0; x < width; x++)
    {
      gaps[x] = 0;
      for (int y = 0; y < height; y++)
      {
        if (GetGem(x,y) == null)
        {
          gaps[x] = gaps[x] + 1;
          Debug.Log("Expand for "+gaps[x]);
        }
        else if (gaps[x] > 0)
        {
          Debug.Log("Shift for "+gaps[x]);
          SwapGems(x ,y, x, y - gaps[x]);
        }
      }
    }
    string debug = "|";
    foreach (int g in gaps)
    {
      debug += g + "|";
    }
    Debug.Log(debug);
    
    return gaps;
  }
  
  public void Print()
  {
    string result = "";
    for (int i = 0; i < width; i++)
    {
      for (int k = 0; k < height; k++)
      {
        if (gems[k * width + i] == null)
          result += "+";
        else
          result += (int) gems[k * width + i].kind;
      }
      result += "\r\n";
    }
    
    Debug.Log(result);
  }

}