﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR.WSA.WebCam;

public static class LineMatcher
{
  private static bool[,] map;
  
  public static bool GetPopulation(this Gem root, out List<Gem> population)
  {

    // Find which coordinates has the root gem
    int root_x, root_y;
    root.parent.GetGemCoords(root, out root_x, out root_y);
    
    // Prepare map of checked gems. Put true in the root position, everything else is false
    map = new bool[root.parent.width,root.parent.height];
    for (int i = 0; i < root.parent.width; i++)
      for (int k = 0; k < root.parent.height; k++)
      {
        map[i, k] = (i == root_x && k == root_y);
      }
    
    // Population is a new list
    population = new List<Gem>();
    List<Gem> oldgeneration = new List<Gem>();
    List<Gem> generation = new List<Gem>();
    List<Gem> nextgeneration = new List<Gem>();

    // If root for some reason has no generation, drop this shit
    if (!root.GetGeneration(out generation))
    {
      return false;
    }
    oldgeneration.AddRange(generation.ToArray());
    population.AddRange(generation);

    // Escape from infinite loop
    bool escape = false;
    int failsafeEscape = 255;


    while (!escape || --failsafeEscape < 0)
    {
      // For previous generation, get generation of each gem and add it to the next generation
      foreach (Gem gem in oldgeneration)
      {
        gem.GetGeneration(out generation);
        nextgeneration.AddRange(generation.ToArray());
      }
      // So, if previous generation has no next generation, escape from this loop
      if (nextgeneration.Count == 0)
        escape = true;
      
      // Add next generation to the population, as it is our future
      population.AddRange(nextgeneration.ToArray());
      // Kill jews
      oldgeneration.Clear();
      // Jews are niggers
      oldgeneration.AddRange(nextgeneration.ToArray());
      // Kill niggers
      nextgeneration.Clear();
    }

    return true;
  }

  // Flood from the root in four directions, and return a collection of gems of same kind as a root
  public static bool GetGeneration(this Gem root, out List<Gem> population){
    population = new List<Gem>();
    // Horizontal generation
    List<Gem> hGeneration = new List<Gem>();
    // Vertical generation
    List<Gem> vGeneration = new List<Gem>();

    hGeneration = root.FloodHorizontal();
    vGeneration = root.FloodVertical();
    
    population.AddRange(hGeneration.ToArray());
    population.AddRange(vGeneration.ToArray());
    
    
    // If this gem has no vertical and horizontal generations, this will return false
    return (hGeneration.Count > 0 || vGeneration.Count > 0);
  }

  
  // Flood from root to the left and right
  public static List<Gem> FloodHorizontal(this Gem rootGem){
      List<Gem> generation = new List<Gem>();
      generation.AddRange(rootGem.FloodLeft());
      generation.AddRange(rootGem.FloodRight());

      // Generation
      return generation;
  }

  public static List<Gem> FloodLeft(this Gem rootGem){
    Matrix rootMatrix = rootGem.parent;
    List<Gem> generation = new List<Gem>();

    // Try get root position in the matrix
    int start_x, start_y;
    if (!rootMatrix.GetGemCoords(rootGem, out start_x, out start_y))
      throw new UnityException("This particular gem is not attached to the current matrix");

    // From root to the left
    if (start_x > 0)
    for (int x = start_x - 1; x >= 0; x--){
      // If next gem is not checked yet, and is same as root gem
      if (!map[x,start_y] && rootGem.IsSame(rootMatrix.GetGem(x, start_y)))
      {
        generation.Add(rootMatrix.GetGem(x, start_y));
        map[x, start_y] = true;
      }
      else
      {
        break;
      }
    }

    // Generation
    return generation;
  }
  public static List<Gem> FloodRight(this Gem rootGem){
    Matrix rootMatrix = rootGem.parent;
    List<Gem> generation = new List<Gem>();

    // Try get root position in the matrix
    int start_x, start_y;
    if (!rootMatrix.GetGemCoords(rootGem, out start_x, out start_y))
      throw new UnityException("This particular gem is not attached to the current matrix");

    // From root to the right
    if (start_x < rootMatrix.width - 1)
    for (int x = start_x + 1; x < rootMatrix.width; x++){
      // If next gem is not checked yet, and is same as root gem
      if (!map[x,start_y] && rootGem.IsSame(rootMatrix.GetGem(x, start_y)))
      {
        generation.Add(rootMatrix.GetGem(x, start_y));
        map[x, start_y] = true;
      }
      else
      {
        break;
      }
    }

    // Generation
    return generation;
  }


  public static List<Gem> FloodVertical(this Gem rootGem){
      List<Gem> generation = new List<Gem>();
      generation.AddRange(rootGem.FloodUp());
      generation.AddRange(rootGem.FloodDown());

      // Generation
      return generation;
  }

  public static List<Gem> FloodUp(this Gem rootGem){
    Matrix rootMatrix = rootGem.parent;
    List<Gem> generation = new List<Gem>();

    // Try get root position in the matrix
    int start_x, start_y;
    if (!rootMatrix.GetGemCoords(rootGem, out start_x, out start_y))
      throw new UnityException("This particular gem is not attached to the current matrix");

    // From root to the top
    if (start_y < rootMatrix.height - 1)
    for (int y = start_y + 1; y < rootMatrix.height; y++){
      // If next gem is not checked yet, and is same as root gem
      if (!map[start_x,y] && rootGem.IsSame(rootMatrix.GetGem(start_x, y)))
      {
        generation.Add(rootMatrix.GetGem(start_x, y));
        map[start_x, y] = true;
      }
      else
      {
        break;
      }
    }

    // Generation
    return generation;
  }
  public static List<Gem> FloodDown(this Gem rootGem){
    Matrix rootMatrix = rootGem.parent;
    List<Gem> generation = new List<Gem>();

    // Try get root position in the matrix
    int start_x, start_y;
    if (!rootMatrix.GetGemCoords(rootGem, out start_x, out start_y))
      throw new UnityException("This particular gem is not attached to the current matrix");

    // From root to the bottom
    if (start_y > 0)
    for (int y = start_y - 1; y >= 0; y--){
      // If next gem is not checked yet, and is same as root gem
      if (!map[start_x,y] && rootGem.IsSame(rootMatrix.GetGem(start_x, y)))
      {
        generation.Add(rootMatrix.GetGem(start_x, y));
        map[start_x, y] = true;
      }
      else
      {
        break;
      }
    }

    // Generation
    return generation;
  }
}