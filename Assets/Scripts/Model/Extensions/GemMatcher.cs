﻿using UnityEngine;

public static class GemMatcher {

    //HELLO EXTENSION METHODS =)
    
    // Tell if gem has vertical neighbours of same kind
    public static bool HasVerticalNeighbours(this Gem gem)
    {
        if (gem == null)
            return false;
        
        // If gem has no parent, return false
        if (gem.parent == null)
            return false;

        // Find gem coordinates in it's parent matrix. If matrix can not find this gem, return false
        int x, y;
        if (!gem.parent.GetGemCoords(gem, out x, out y))
            return false;

        // Check if gem is located on edge, if it is - return false
        if (y <= 0 || y >= gem.parent.height - 1)
            return false;

        // Check if gem has same two gems on the top and on the bottom
        if (gem.IsSame(gem.parent.GetGem(x, y - 1)) && gem.IsSame(gem.parent.GetGem(x, y + 1)))
            return true;

        // If nothing works - return false
        return false;
    }

    // Tell if gem has horizontal neighbours of same kind
    public static bool HasHorizontalNeighbours(this Gem gem){
        // If gem has no parent, return false
        if (gem.parent == null)
            return false;

        // Find gem coordinates in it's parent matrix. If matrix can not find this gem, return false
        int x, y;
        if (!gem.parent.GetGemCoords(gem, out x, out y))
            return false;

        // Check if gem is located on edge, if it is - return false
        if (x <= 0 || x >= gem.parent.width - 1)
            return false;

        // Check if gem has same two gems on the left and on the right
        if (gem.IsSame(gem.parent.GetGem(x - 1, y)) && gem.IsSame(gem.parent.GetGem(x + 1, y)))
            return true;

        // If nothing works - return false
        return false;
    }
}
