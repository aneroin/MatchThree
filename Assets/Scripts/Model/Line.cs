﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

public class Line {
    // Gems in this line
    List<Gem> gems = new List<Gem>();

    // Constructor
    public Line (List<Gem> gems){
        this.gems = gems;
    }

    // This is a chain method, you can push to the line like `line.Push(gem).Push(gem).Push(gem)`
    public Line Push(Gem gem){
        gems.Add(gem);
        return this;
    }

    public List<Gem> Gems()
    {
        return gems;
    }

    
    // Here you call gem destroy, and calculate a score. Score is returned as int
    public int Burn(){
        int score = 0;
        int count = gems.Count;
        int multiplier;

        // If line is bigger than 5 gems, multiply score by 3x
        if (count > 5){
            multiplier = 3;
            // If line is bigger than 3 gems, multiply score by 2x
        } else if (count > 3) {
            multiplier = 2;
            // Standard line gets 1x multiplier :(
        } else {
            multiplier = 1;
        }

        // Calculate score
        score = count * multiplier;

        /*** Start burning coroutine, for instance
        ** Go througn the gems list, and burn each gem
    
        BURN BURN BURN
    
        ** Return score to the user
        ***/

        foreach (var gem in gems)
        {
            gem.parent.DestroyGem(gem);
        }

        // This may be used later, but we only use it in the Print =/
        return score;
    }

    public void Print(){
        // Instead of printing, use Burn and display result on canvas if you want
        Debug.Log("This line gives you "+Burn()+" points!");
    }
}