﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class MatchGame : MonoBehaviour {
	public int width;
	public int height;
	private Matrix matrix;
	private List<GemView> views;
	public GameObject GemViewPrefab;
	public bool controlled = true;

	[Header("Text for score")]
	public Text scoreText;
	private int score = 0;
	
	private Gem selectedOne;
	private Gem selectedTwo;

	[Header("first gem selected")]
	public bool S1;
	[Header("second gem selected")]
	public bool S2;

	// Singleton!

	private static MatchGame _Instance;

	public static MatchGame Instance
	{
		get { return _Instance; }
	}

	void Awake()
	{
		if (_Instance != null && _Instance != this)
			Destroy(this);
		else
			_Instance = this;
	}

	// Use this for initialization
	void Start()
	{
		// Recenter camera fast
		Camera.main.transform.position += new Vector3(width / 2f -0.5f, height / 2f -0.5f, 0f);
		Camera.main.orthographicSize = Mathf.Max(width, height);

		// Build views collection
		views = new List<GemView>(width * height);

		// Matrix init
		matrix = new Matrix(width, height);

		// Bind event handlers
		matrix.OnSpawn = this.OnSpawnGem;
		matrix.OnDestroy = this.OnDestroyGem;
		matrix.OnSwap = this.OnSwapGems;

		// Generate random board
		for (int i = 0; i < width; i++)
		{
			for (int k = 0; k < height; k++)
			{
				matrix.PutGem(i, k, Gem.Random(matrix));
			}
		}

		// Check board first time after generation
		StartCoroutine(Pass());
	}


	// Withdraw control, start game cycle, destroy lines, return control
	IEnumerator Pass()
	{
		controlled = false;
		yield return new WaitForSeconds(0.5f);

		List<Line> lines = ExtractLines();

		if (lines.Count > 0)
		{

			foreach (Line line in lines)
			{
				// Here, behind printing, lines are burned and scores are printed to the debug console. Use here line.Burn() which returns int score if you want.
				score += line.Burn();
				scoreText.text = "Score :" + score;
				yield return new WaitForSeconds(0.2f);
			}

			Debug.Log("FallGems");
			int[] amounts = matrix.FallGems();

			yield return new WaitForSeconds(0.25f);

			for (int x = 0; x < width; x++)
			{
				if (amounts[x] > 0)
				for (int i = 0; i < amounts[x]; i++)
				{
					matrix.PutGem(x, height - 1 - i, Gem.Random(matrix));
					yield return new WaitForSeconds(0.05f);
				}
			}

			StartCoroutine(Pass());
		}

		controlled = true;
		yield return true;
	}


	// Extract lines from the matrix
	List<Line> ExtractLines()
	{
		if (matrix == null)
			Debug.Log("MATRIX WTF");

		// Get gems, which have at lest 2 neighbours of same kind
		List<Gem> candidates = matrix.GetCandidateGems();

		// build populaion of gems in line
		List<Gem> population = new List<Gem>();
		// build list of lines
		List<Line> lines = new List<Line>();

		// Candidates Elimination
		if (candidates.Count > 0)
		{
			// Mark checked candidate elements, to not include them into the next lines
			bool[,] checkedCandidates = new bool[matrix.width, matrix.height];

			for (int i = 0; i < candidates.Count; i++)
			{
				int x, y;
				matrix.GetGemCoords(candidates[i], out x, out y);
				// If candidate wasn't checked, and it has population
				if (!checkedCandidates[x,y] && candidates[i].GetPopulation(out population))
				{
					// Exclude candidate from next checks
					checkedCandidates[x, y] = true;
					// Add population to a line
					Line thisLine = new Line(population).Push(candidates[i]);
					
					// Exclude all candidates, which are part of an existent line
					foreach (Gem candidate in thisLine.Gems())
					{
						if (candidates.Contains(candidate))
						{
							int l, m;
							matrix.GetGemCoords(candidate, out l, out m);
							checkedCandidates[l, m] = true;

						}

					}
					// Save line in a list
					lines.Add(thisLine);
				}
			}
		}
		return lines;
	}

	// Handle gem spawn in the matrix, by instantiating some game object
	void OnSpawnGem(Gem gem)
	{
		int x, y;
		gem.parent.GetGemCoords(gem, out x, out y);
		GameObject go = Instantiate(GemViewPrefab, new Vector3((float)x, (float)y, 0f), Quaternion.identity);
		views.Add(go.GetComponent<GemView>().FromGem(gem));
	}

	// Handle gem destroy
	void OnDestroyGem(Gem gem)
	{
		GemView view = views.Single(v => v.gemReference == gem);
		views.Remove(view);
		DestroyImmediate(view.gameObject);
	}

	// Handle gem swapping
	void OnSwapGems(int one_x, int one_y, int another_x, int another_y)
	{
		Debug.Log(string.Format("Swap {0}:{1} to {2}:{3}",one_x, one_y, another_x, another_y));

		Gem one = matrix.GetGem(one_x, one_y);
		Gem another = matrix.GetGem(another_x, another_y);

		if (one != null)
			views.Single(v => v.gemReference == one).gameObject.transform.position = new Vector3((float)another_x, (float)another_y, 0f);

		if (another != null)
			views.Single(v => v.gemReference == another).gameObject.transform.position = new Vector3((float)one_x, (float)one_y, 0f);
	}

	// Selecting gems
	public void OnGemSelected(Gem gem)
	{
		if (!controlled)
			return;

		if (selectedOne == null)
		{
			S1 = true;
			selectedOne = gem;
			return;
		} else if (selectedOne == gem)
		{
			S1 = S2 = false;
			selectedOne = null;
			selectedTwo = null;
			return;
		}

		if (selectedTwo == null)
		{
			S2 = true;
			selectedTwo = gem;
			if (matrix.TrySwapGems(selectedOne, selectedTwo))
			{
				if (ExtractLines().Count == 0)
					matrix.TrySwapGems(selectedOne, selectedTwo);
				S1 = S2 = false;
				selectedOne = null;
				selectedTwo = null;
				StartCoroutine(Pass());
			}
			else
			{
				S1 = S2 = false;
				selectedOne = null;
				selectedTwo = null;
			}
			return;
		} else if (selectedTwo == gem)
		{
			S2 = false;
			selectedTwo = null;
			return;
		}
	}

}
