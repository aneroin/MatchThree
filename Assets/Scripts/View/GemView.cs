﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer))]
public class GemView : MonoBehaviour, IPointerClickHandler
{
	// Display coordinates on this label
	public TextMesh coords;
	
	// Reference to the gem model
	private Gem _gemReference;
	public Gem gemReference
	{
		get { return _gemReference; }
	}
	
	// View renderer
	private SpriteRenderer _sprite;
	
	// Use this for initialization
	void Awake ()
	{
		_sprite = GetComponent<SpriteRenderer>();
		coords = GetComponentInChildren<TextMesh>();
	}

	// Create view color from the model
	public GemView FromGem(Gem gem)
	{
		_gemReference = gem;
		
		// Here you can set color for any gem kind you have in the Kind enum. Can be static color from the list, or new color
		switch (_gemReference.kind)
		{
			case Gem.Kind.blue:
			{
				_sprite.color = Color.blue;
				break;
			}
			case Gem.Kind.green:
			{
				_sprite.color = Color.green;
				break;
			}
			case Gem.Kind.red:
			{
				_sprite.color = Color.red;
				break;
			}
			case Gem.Kind.yellow:
			{
				_sprite.color = new Color(0.75f, 0.45f, 0f); // It's how you can put your color
				break;
			}
		}
		StartCoroutine(UpdateLabel());
		return this;
	}

	// Twice a second, update coordinates on the label
	IEnumerator UpdateLabel()
	{
		while (true)
		{
			int x, y;
			_gemReference.parent.GetGemCoords(_gemReference, out x, out y);
			coords.text = x + ":" + y;
			yield return new WaitForSeconds(0.5f);
		}
	}
	
	
	// On pointer click, tell game controller this gem is selected
	public void OnPointerClick(PointerEventData eventData)
	{
		Debug.Log("clicked on gem");
		MatchGame.Instance.OnGemSelected(_gemReference);
	}
}
